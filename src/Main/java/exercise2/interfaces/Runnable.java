package exercise2.interfaces;

public interface Runnable {

    public String run();
}
