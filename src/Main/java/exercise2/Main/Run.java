package exercise2.Main;

public class Run {

    public static void main(String[] args) {
        Animals[] animalses = {new Cat("Barsik","orange"), new Sparrow("Chick-chirik","black"), new Lizard("Oscar","green"), new Salmon("Solomon","grey"),};
        for (int i = 0; i < animalses.length; i++) {
            animalses[i].voice();
        }
    }
}
