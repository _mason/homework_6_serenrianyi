package exercise2.Main;

import exercise2.enums.TypeOfAnimal;

public abstract class Animals {

    protected String name;
    protected int countOfLegs;
    protected int countOfWings;
    protected String color;
    protected TypeOfAnimal typeOfAnimal;

    public abstract void voice();
}
