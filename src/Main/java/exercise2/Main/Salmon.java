package exercise2.Main;

import exercise2.enums.TypeOfAnimal;
import exercise2.interfaces.Swimable;

public class Salmon extends Animals implements Swimable {

    public Salmon(String name,String color){
        this.name=name;
        this.color=color;
        this.countOfLegs=0;
        this.countOfWings=0;
        this.typeOfAnimal=TypeOfAnimal.FISH;
    }

    @Override
    public String swim() {
        return "I can Swim!";
    }

    @Override
    public void voice() {
        System.out.println("My name is " + name + ". My color is " + color + ". I have " + countOfLegs + " legs. I have " + countOfWings + " wings. " + swim() + " Who am I?");
    }
}
