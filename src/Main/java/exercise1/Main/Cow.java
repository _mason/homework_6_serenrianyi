package exercise1.Main;

import exercise1.enums.Pets;

public class Cow extends Pet {

    public Cow(String nameOfPets) {
        this.typeOfPets = Pets.Cow;
        this.nameOfPets = nameOfPets;
    }

    @Override
    public void voice() {
        System.out.println("I am " + typeOfPets + ". My name is " + nameOfPets + ". My voice is mooo!");
    }
}
