package exercise3.Main;

import exercise3.enums.PhoneType;

public class Phone {

    protected String producer;// - производитель
    protected String model;// - модель
    protected int numberOfSim=1;// - количество сим-карт
    protected Boolean hasCamera;// - имеет ли камеру
    protected String color;// - цвет
    protected PhoneType phoneType;//- тип корпуса,

    public Phone(String producer, String model, int numberOfSim, Boolean hasCamera, String color, PhoneType phoneType){
        this.producer=producer;
        this.model=model;
        this.numberOfSim=numberOfSim;
        this.hasCamera=hasCamera;
        this.color=color;
        this.phoneType=phoneType;
    }

    public void setProducer(String producer) {
        this.producer = producer;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setNumberOfSim(int numberOfSim) {
        this.numberOfSim = numberOfSim;
    }

    public void setHasCamera(Boolean hasCamera) {
        this.hasCamera = hasCamera;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setPhoneType(PhoneType phoneType) {
        this.phoneType = phoneType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Phone phone = (Phone) o;

        if (numberOfSim != phone.numberOfSim) return false;
        if (!producer.equals(phone.producer)) return false;
        if (!model.equals(phone.model)) return false;
        if (!hasCamera.equals(phone.hasCamera)) return false;
        return phoneType == phone.phoneType;

    }

    @Override
    public int hashCode() {
        int result = producer.hashCode();
        result = 31 * result + model.hashCode();
        result = 31 * result + numberOfSim;
        result = 31 * result + hasCamera.hashCode();
        result = 31 * result + phoneType.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Phone{" +
                "producer='" + producer + '\'' +
                ", model='" + model + '\'' +
                ", numberOfSim=" + numberOfSim +
                ", hasCamera=" + hasCamera +
                ", color='" + color + '\'' +
                ", phoneType=" + phoneType +
                '}';
    }
}
