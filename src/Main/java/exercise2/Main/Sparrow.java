package exercise2.Main;

import exercise2.enums.TypeOfAnimal;
import exercise2.interfaces.Flyable;

public class Sparrow extends Animals implements Flyable {

    public Sparrow(String name,String color){
        this.name=name;
        this.color=color;
        this.countOfLegs=2;
        this.countOfWings=2;
        this.typeOfAnimal=TypeOfAnimal.BIRD;
    }

    @Override
    public String fly() {
        return "I can Fly!";
    }

    @Override
    public void voice() {
        System.out.println("My name is " + name + ". My color is " + color + ". I have " + countOfLegs + " legs. I have " + countOfWings + " wings. " + fly() + " Who am I?");
    }
}
