package exercise3.Main;

import exercise3.enums.PhoneType;

public class Run {

    public static void main(String[] args) {
        Shop shop=new Shop();
        Phone randomPhone = new Phone("China","China 4", 2, true,"Red",PhoneType.SENSOR);
        Phone[] ppap = {new Phone("China","China 1", 1, true,"Black",PhoneType.BLOCK), new Phone("China","China 2", 2, true,"Yelow",PhoneType.FLIP)};
        shop.newDelivery(ppap);
        shop.printPhonesInStock();
        System.out.println("");
        Phone[] ppap1 = {new Phone("China","China 3", 1, false,"White",PhoneType.SLIDER),new Phone("China","China 4", 2, true,"Red",PhoneType.SENSOR)};
        shop.newDelivery(ppap1);
        shop.printPhonesInStock();

        int count=0;
        for (int i = 0; i < shop.phonesInStock.length; i++) {
            if ( shop.phonesInStock[i].equals(randomPhone) ) {
                count++;
                System.out.println("phone in stock");
            }
        }
        if (count == 0) {
            System.out.println("the phone is not in stock");
        }
    }
}
