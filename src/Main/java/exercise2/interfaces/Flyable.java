package exercise2.interfaces;

public interface Flyable {

    public String fly();
}
