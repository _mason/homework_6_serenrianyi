package exercise1.Main;


import exercise1.enums.Pets;

public class Cat extends Pet {

    public Cat(String nameOfPets){
        this.typeOfPets= Pets.Cat;
        this.nameOfPets=nameOfPets;
    }

    @Override
    public void voice() {
        System.out.println("I am " + typeOfPets +". My name is " + nameOfPets +". My voice is meow");
    }
}
