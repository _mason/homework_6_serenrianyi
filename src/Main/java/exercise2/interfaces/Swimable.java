package exercise2.interfaces;

public interface Swimable {

    public String swim();

}
