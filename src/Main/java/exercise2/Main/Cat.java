package exercise2.Main;

import exercise2.enums.TypeOfAnimal;
import exercise2.interfaces.Runnable;

public class Cat extends Animals implements Runnable {

    public Cat(String name,String color){
        this.name=name;
        this.color=color;
        this.countOfLegs=4;
        this.countOfWings=0;
        this.typeOfAnimal=TypeOfAnimal.MAMMAL;
    }

    @Override
    public String run() {
        return "I can Run!";
    }

    @Override
    public void voice() {
        System.out.println("My name is " + name + ". My color is " + color + ". I have " + countOfLegs + " legs. I have " + countOfWings + " wings. " + run() + " Who am I?");
    }
}
