package exercise2.Main;

import exercise2.enums.TypeOfAnimal;
import exercise2.interfaces.Runnable;
import exercise2.interfaces.Swimable;

public class Lizard extends Animals implements Runnable, Swimable {

    public Lizard(String name,String color){
        this.name=name;
        this.color=color;
        this.countOfLegs=4;
        this.countOfWings=0;
        this.typeOfAnimal=TypeOfAnimal.REPTILIA;
    }

    @Override
    public String run() {
        return "I can Run!";
    }

    @Override
    public String swim() {
        return "I can Swim!";
    }

    @Override
    public void voice() {
        System.out.println("My name is " + name + ". My color is " + color + ". I have " + countOfLegs + " legs. I have " + countOfWings + " wings. " + run()  + swim() + " Who am I?");
    }
}
