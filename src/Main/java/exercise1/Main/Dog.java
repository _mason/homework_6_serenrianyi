package exercise1.Main;

import exercise1.enums.Pets;

public class Dog extends Pet {

    public Dog(String nameOfPets){
        this.typeOfPets=Pets.Dog;
        this.nameOfPets=nameOfPets;
    }
    @Override
    public void voice() {
        System.out.println("I am " + typeOfPets +". My name is " + nameOfPets +". My voice is woof");
    }
}
