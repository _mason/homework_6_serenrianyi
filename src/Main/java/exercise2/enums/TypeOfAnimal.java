package exercise2.enums;

public enum TypeOfAnimal {

    MAMMAL,     //(млекопитающее)
    REPTILIA,  //(земноводное)
    BIRD,       //(птица)
    FISH        //(рыба)
}
