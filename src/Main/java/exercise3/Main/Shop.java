package exercise3.Main;

public class Shop {

    Phone[] phonesInStock = new Phone[]{};

    public Phone[] getPhonesInStock() {
        return phonesInStock;
    }

    public void newDelivery(Phone[] phonesDelivery){
        Phone[] newPhoneInStock = new Phone[phonesInStock.length+phonesDelivery.length];
        for (int i = 0; i < phonesInStock.length; i++) {
            newPhoneInStock[i]=phonesInStock[i];
        }
        for (int i = phonesInStock.length; i < newPhoneInStock.length; i++) {
            newPhoneInStock[i]=phonesDelivery[i-phonesInStock.length];
        }
        phonesInStock=newPhoneInStock;
    }

    public int countOfSearchPhone(Phone searchPhone){
        int count=0;
        for (int i = 0; i < phonesInStock.length; i++) {
            if (searchPhone==phonesInStock[i]) count++;
        }
        return count;
    }

    public void printPhonesInStock(){
        for (int i = 0; i < phonesInStock.length; i++) {
            System.out.println(phonesInStock[i].toString());
        }
    }
}
